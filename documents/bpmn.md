# Модель бизнес-процессов

![Создание заказа клиентом](https://drive.google.com/uc?export=view&id=1OSTBXL1Tq8HeJLMSC9N5t-hth5AcZsQJ)
![Редактирование заказа клиентом](https://drive.google.com/uc?export=view&id=1NrEdA97JVpM7ewD-DY_9uojvKyQl-6fp)
![Регистрация клиента](https://drive.google.com/uc?export=view&id=1-RlYeeQrZU2spQw7dWkugbzeYN6VFiAY)
![Начало сессии водителя](https://drive.google.com/uc?export=view&id=1_5F4d3LhzX9sJj4rn1uoy76LoOSxEqhj)
![Завершение сеанса водителя](https://drive.google.com/uc?export=view&id=1LuszcUOP9yG8Wt601uzwqiuaS9iqzSN3)
![Закрытие заказа](https://drive.google.com/uc?export=view&id=1d289zKyr0CtTCHAhkVi_YeytsbqASfmk)
![Просмотр таксопарка](https://drive.google.com/uc?export=view&id=1MYpAroxMHfMi_n2FtzBongr2DSVk5SZs)
![Деактивация водителя](https://drive.google.com/uc?export=view&id=14VXlpZIiAPSV7smA-WoivIiIPoEF3DjE)
![Добавление таксопарка](https://drive.google.com/uc?export=view&id=1iDFlFAB7odCgkWETzXtan3XDoM1ttoQ0)
![Регистрация водителя таксопарка](https://drive.google.com/uc?export=view&id=14Kyaczl47i4Tnv7WRS6IAPhJOp1r43BI)
![Просмотр истории заказов](https://drive.google.com/uc?export=view&id=1h6mQWDLzskOf7p3uI3KHbmHm8C3g5CPn)
![Продление договора обслуживания](https://drive.google.com/uc?export=view&id=1N--bipCVm1G2og5KZ8yy10tHqH60fEdv)
![Прекращение договора](https://drive.google.com/uc?export=view&id=197mrMozpIciDBn91f9UxW7G84Hfm0Y_A)
![Добавление новой машины](https://drive.google.com/uc?export=view&id=1j-zrB595Fl9lNEGXw-HZPLiaHHDlHK9h)
![Уведомление клиента о прибытии машины](https://drive.google.com/uc?export=view&id=15o1hqE0JGkjg8oHSUFLJuwafbPohzwol)
![Начало поездки](https://drive.google.com/uc?export=view&id=1OOoKXV4Ii45GXSRrmeQYxn7_YUPZQWsG)
![Получение аналитики по заказам таксопарка](https://drive.google.com/uc?export=view&id=1AH5NVNpdUT0mJFrRk-yqZqLoKJ5Z5ZpL)
![Редактирование данных водителя](https://drive.google.com/uc?export=view&id=1Ghrra1tMXof4EiQbzJnugr_PwF6Hv5M8)
![Активация водителя](https://drive.google.com/uc?export=view&id=1Pl4GwaLNo4Ywr-kWbxbKYU5r9LhK4fdU)
![Карточка водителя](https://drive.google.com/uc?export=view&id=1KGtfL7rFDKpagMFG0PrGL6zkqSxDrHJp)


